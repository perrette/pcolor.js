/*
 Class to make a pcolor-like figure
 Requires d3
*/

if (d3 === undefined) {
    alert("d3 must be included before pcolor.js");
}

// plot variable
var plt = {};

// classes defined by pcolor
plt.class = {};
plt.class.pcolor = "pcolor";
plt.class.infobox = "infobox";
plt.class.drawing = "drawing";
plt.class.drawing = "colorbar";
plt.class.xaxes = "xaxis";
plt.class.yaxes = "yaxis";

// color palette
plt.defaultPalette = 
    ['rgb(215,48,39)','rgb(244,109,67)','rgb(253,174,97)','rgb(254,224,144)','rgb(255,255,191)','rgb(224,243,248)','rgb(171,217,233)','rgb(116,173,209)','rgb(69,117,180)'].reverse();

plt.defaultWidth = "960" 
plt.defaultHeight= "500"

// round number
plt.round = function(num, prec) {
  if (prec === undefined) {
    prec = 2;
  }
  fac = Math.pow(10, prec);
  return Math.round(num * fac) / fac;
};


// Make a pcolor plot following conventions from 
// http://bost.ocks.org/mike/chart

plt.pcolor = function() {

  // can be connected by getter / setter below
  // from the outside (see below)
    
  // data
  var x = [], y=[], z=[], datar = [];
  
  // missing value flag: NaN by default
  var missing = NaN;
  var mask;  // permanent mask to filter out, undefined by default
    
  // config
  var xrange = "auto", yrange = "auto", crange = "auto";
  var palette = plt.defaultPalette;

  var zoomable = false;
  var hoverable = true;

  var resetZoom;

  // svg properties
  var width, height;
  var iwidth, iheight;
  var xrangep, yrangep;

   // padding for the inner frame
  var padding = {
    right : 100,
    left : 50,
    top : 15,
    bottom : 30
  };

  var rectsize;

  var rootSvg, mapSvg; // mapSvg for the map, for zooming
  
  var colorbar, colorbarSvg;

  // realtime variables (made from config)
  var xAxisScale, yAxisScale, colorScale;
  var xAxis, Yaxis, xAxisSvg, yAxisSvg;

  function my(selection) {

    if (datar === undefined) {
      console.error("Please define data via .data() method (properties x, y, z expected)");
    };

    rootSvg = selection // assume selection is svg

    // make sure width and height are defined
    width = rootSvg.attr("width"); // || svg.attr("width", plt.defaultWidth).attr("width"); 
    height = rootSvg.attr("height"); // || svg.attr("height", plt.defaultHeight).attr("height");

    // inner width (drawing area)
    iwidth = width - (padding.left+padding.right);
    iheight = height - (padding.top+padding.bottom);

    // ranges in pixel of the inner frame 
    xrangep = [padding.left, padding.left + iwidth];
    yrangep = [padding.top+iheight, padding.top];

    // inner frame for the map
    mapSvg = rootSvg.append("g")
      .classed(plt.class.pcolor, true)

    // filter numeric values prior to computing colorbar
    function isNumber(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    };
    var numz = z.filter(function(d) {return isNumber(d)});

    // axis and color range
    xranged = xrange !== 'auto' ? xrange : d3.extent(x);
    yranged = yrange !== 'auto' ? yrange : d3.extent(y);
    cranged = crange !== 'auto' ? crange : d3.extent(numz);

    // define axis scales
    xAxisScale = d3.scale.linear()
    .domain(xranged) // from the data
    .range(xrangep); // on the figure

    yAxisScale = d3.scale.linear()
    .domain(yranged)
    .range(yrangep)

    // define colorscale
    var cextent = cranged[1]-cranged[0]; 
    var domain = [];
    var n = palette.length;
    for (var i=0; i<n; i++) {
      domain[i] = cranged[0] + cextent*i/(n-1);
    };

    colorScale = d3.scale.linear()
    .domain(domain)
    .range(palette)

    // round the ranges if automatic
    if (xrange === 'auto') xAxisScale.nice();
    if (yrange === 'auto') yAxisScale.nice();
    if (crange === 'auto') colorScale.nice();

    // return rectangle size based on data
    var dw = Math.abs(xAxisScale(x[1]) - xAxisScale(x[0]));
    var dh = Math.abs(yAxisScale(y[1]) - yAxisScale(y[0]));
    // Round at 2 decimals
    dw = plt.round(dw);
    dh = plt.round(dw);
    rectsize = {width: dw, height: dh};

    // If zoom, append a zoom group to draw into
    if (zoomable) {

      function zoom() {
        mapSvg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
        my.refreshAxes();
        // hide what is outside the axes
        // rects.style("visibility", setVisible);  // works but is too heavy
      }

      var zm = d3.behavior.zoom().x(xAxisScale).y(yAxisScale).scaleExtent([1, 8]).on("zoom", zoom);

      mapSvg = mapSvg
        .append("g")
         .attr("class","zoomable")
         .call(zm)
        .append("g");

      mapSvg.append("rect")
        .attr("class", "overlay")
        .attr("width", iwidth)
        .attr("height", iheight)
        .attr("x", padding.left)
        .attr("y", padding.top);

      //function resetZoom() {
      //  zm.scale(1);
      //  zm.translate([0,0]);
      //};
    };

    // rects selection
    var rects = mapSvg.selectAll("rect")
      .data(datar); //, function(d, i){ return i+'_'+d.value;})  // extend identifyier to foster update

    // Append rectangles
    enter = rects
    .enter().append("rect")
    // set via CSS?
    .attr("width", rectsize.width) // CCS?
    .attr("height", rectsize.height) // CCS?
    // set via transform
    //.attr("x", function(d) { return xAxisScale(d.x) })
    //.attr("y", function(d) { return yAxisScale(d.y) })
    .attr("x", function(d) { return xAxisScale(d.x) })
    .attr("y", function(d) { return yAxisScale(d.y) })

    if (hoverable) enter.on("mouseover", my.mouseover);
    
    // Only update filling, not the position
    //rects
    rects
    .style("visibility", setVisible)
    .attr("fill", function(d) { 
      if (isNumber(d.value)) {
        return colorScale(d.value);
      } else {
        return undefined;
      } })
    .attr("stroke", function(d) {
      if (isNumber(d.value)) {
        return colorScale(d.value);
      } else {
        return undefined;
      } });
    
    //var rects = rootSvg.selectAll("rect")
    rects.exit()
      .remove();
  
    return my;

  };

  // apply to a d3 element
  function setVisible(d) {
    //var last_rows = (d.x === x[x.length-1] || d.y === y[0]);
    //var outside_axes = (d.x < xAxisScale.domain[0] || d.x > xAxisScale.domain[1] 
    //                   || d.y < yAxisScale.domain[0] || d.y > yAxisScale.domain[1] );
    //

    // initally thought to activate while zooming, but is too heavy
    var xp = xAxisScale(d.x), yp = yAxisScale(d.y);
    var outside_axes = (xp < xrangep[0] || xp > xrangep[1] - rectsize.width
                     || yp > yrangep[0] - rectsize.height || yp < yrangep[1]);

    var nans = d.value === missing;
    //if (nans || outside_axes) {
    if (nans || outside_axes) {
      return "hidden";
      } else {
      return undefined; // visible by default
    };
  };

  // gettter-setter methods

  
  my.data = function(data) {
    if (!arguments.length) return {x:x, y:y, z:z, missing:missing};
    x = data.x || x;
    y = data.y || y;
    z = data.z || z;
    missing = data.missing !== undefined ? data.missing : missing;
    mask = data.mask !== undefined ? data.mask : mask;

    // Convert the data to rectangles
    datar = d3.range(0, z.length).map(function(k) {
      // retrieve i and j indices from linear index
      // modulo y.length
      var j = k % x.length;  
      var i = Math.floor((k - j) / x.length)  ;
      //if (i == NaN) {i = 0};
      // define single rectangle properties
      return {
        "x":x[j], // shift data for 0 origin
        "y":y[i],  // vertical shift
        "value":z[k]
      };

    });

    // remove land elements for speed up
    datar = datar.filter(function(d) {return d.value !== mask;});

    return my; // chained selection
  };

  my.xrange = function(value) {
    if (!arguments.length) return xrange;
    xrange = value;
    return my;
  };

  my.yrange = function(value) {
    if (!arguments.length) return yrange;
    yrange = value;
    return my;
  };

  my.crange = function(value) {
    if (!arguments.length) return crange;
    crange = value;
    return my;
  };

  my.padding = function(value) {
    if (!arguments.length) return padding;
    if (typeof value !== "object") console.error("padding must be an object with props left, right, top, bottom");
    padding = value;
    return my;
  };

  // Now methods that act on the rootSvg

  my.addAxes = function() {

    // Add axes
    xAxis = d3.svg.axis()
    .scale(xAxisScale)
    .orient("bottom")
    .ticks(10);

    yAxis = d3.svg.axis()
    .scale(yAxisScale)
    .orient("left")
    .ticks(5);

    xAxisSvg = rootSvg.append("g")
    .attr("class","axis x")
    .attr("transform", "translate(0," + yrangep[0] + ")")
    .call(xAxis);

    yAxisSvg = rootSvg.append("g")
    .attr("class","axis y")
    .attr("transform", "translate(" + xrangep[0] + ",0)")
    .call(yAxis);

    return my;

  };

  // update axes
  my.refreshAxes = function() {
     //my.delAxes()
     //my.addAxes()
     xAxis.scale(xAxisScale);
     yAxis.scale(yAxisScale);
     xAxisSvg.call(xAxis);
     yAxisSvg.call(yAxis);
  };

  my.delAxes = function() {
    // remove existing axes
    //d3.selectAll(".axis").remove();
    xAxisSvg.remove();
    yAxisSvg.remove();
    return my;
  };

  my.addColorbar = function() {
    colorbar = Colorbar()
    .origin([xrangep[1]+padding.right*0.2, yrangep[1]]) 
    .scale(colorScale)
    .barlength(Math.abs(yrangep[1] - yrangep[0]) -1)
    .thickness(20); // bug without -1

    // define new colorbarSvg
    //colorbarSvg = rootSvg.append("g")
    //.attr("class", "colorbar")
    ////.attr("id", cid)
    //.call(colorbar);
    rootSvg.call(colorbar);
    colorbarSvg = rootSvg.selectAll(".g.colorbar");
    return my;
  };

  my.refreshColorbar = function() {
    //my.addColorbar();
      
    // doing below adds a smooth transition
    colorbar.scale(colorScale); // update scale
    // Apparently just calling colobar does not work for update
    // (only change axes, not colors)
    //colorbarSvg.call(colorbar); // redraw it
    colorbarSvg.remove();
    rootSvg.call(colorbar);
    colorbarSvg = rootSvg.selectAll(".g.colorbar");
    return my;
  };

  my.delColorbar = function() {
    if (colorbarSvg !== undefined) colorbarSvg.remove();
    return my;
  };

  // FOR DEBUGGING
    
  // Make a few fields accessible from the outside
  my.svg = function() {return rootSvg;};

  my.colorbarSvg = function() {
    return colorbarSvg;
  };

  // Non-chain methods
  my.x = function(value) {
    return xAxisScale(value);
  };

  my.y = function(value) {
    return yAxisScale(value);
  };
  my.colorScale = function(value) {
    return colorScale(value);
  };

  // USER INTERACTIONS
    
  // When the mouse is passed over the figure
  my.mouseover = function(d) {
    var message=d.x+"E, "+d.y+"N : "+d.value+" m";
    infobox.text(message);
  };

  my.resetZoom = function() {
    return resetZoom();
  };

    
  // Add coordinate box when mouse passes over figure
  var infobox ; // svg text selection
  my.addInfoBox = function() {

    if (arguments.length === 1) {
      // add textbox directly
      infobox = arguments[0]; 
    } else {
      var x, y;
      // provide box position
      if (arguments.length === 2) {
        x = arguments[0];
        y = arguments[1];
      // default position
      } else {
        if (arguments.length!==0) console.warn("addInfoBox must have 0, 1, or 2 arguments (ignore)")
        var x = xrangep[0]+3;
        var y = yrangep[1]-3;
      };
      infobox = rootSvg.append('text')
        .attr("class", plt.class.infobox)
        .attr("x",x)
        .attr("y",y)
        .text(""); //"User your mouse to explore the map"); // initial text
    };

    return my;
  };

  my.delInfoBox = function() {
    infobox.remove();
    return my;
  };

  my.infobox = function(value) {
    if (!arguments.length) return infobox;
    return infobox;
  };


  // // Apply zoom behaviour to SLR map
  // var zoom = d3.behavior.zoom();
  // d3.behavior.zoom(d3.select("#slr_map"));
  
  return my;

};
